package az.ingress.ms9liquibase.repository;

import az.ingress.ms9liquibase.model.Product;
import az.ingress.ms9liquibase.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository <User, Long> {
}
