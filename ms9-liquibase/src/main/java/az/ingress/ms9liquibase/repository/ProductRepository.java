package az.ingress.ms9liquibase.repository;

import az.ingress.ms9liquibase.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository <Product, Long> {
}
