package az.ingress.ms9liquibase.service;

import az.ingress.ms9liquibase.dto.ProductDto;
import org.springframework.stereotype.Service;


public interface ProductService {
    ProductDto getProductById(Long id);
    ProductDto createProduct(ProductDto dto);
    void deleteProduct (Long id);
    ProductDto updateProduct(ProductDto dto);
}
