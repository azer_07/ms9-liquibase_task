package az.ingress.ms9liquibase.service;

import az.ingress.ms9liquibase.dto.ProductDto;
import az.ingress.ms9liquibase.model.Product;
import az.ingress.ms9liquibase.repository.ProductRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class ProductServiceImpl implements ProductService {
    private final ProductRepository repository;
    private final ModelMapper mapper;

    @Override
    public ProductDto getProductById(Long id) {
        Product byId=repository.getById(id);
        ProductDto dto= mapper.map(byId,ProductDto.class);
        return dto;
    }

    @Override
    public ProductDto createProduct(ProductDto dto) {
        Product product=mapper.map(dto,Product.class);
        Product save=repository.save(product);
        return mapper.map(save,ProductDto.class);
    }

    @Override
    public void deleteProduct(Long id) {
        repository.deleteById(id);

    }

    @Override
    public ProductDto updateProduct(ProductDto dto) {
        Product product=repository.findById(dto.getId()).orElseThrow((()-> new RuntimeException("product is not found")));
        product.setBrand(dto.getBrand());
        product.setCount(dto.getCount());
        product.setPrice(dto.getPrice());
        product.setCreateDate(dto.getCreateDate());
        product.setUniqueCode(dto.getUniqueCode());
        product.setIsActive(dto.getIsActive());
        Product save=repository.save(product);
        return mapper.map(save,ProductDto.class);

    }
}
