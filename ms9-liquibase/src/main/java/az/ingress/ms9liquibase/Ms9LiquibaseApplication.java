package az.ingress.ms9liquibase;

import az.ingress.ms9liquibase.model.Address;
import az.ingress.ms9liquibase.model.Contact;
import az.ingress.ms9liquibase.model.User;
import az.ingress.ms9liquibase.model.UserRole;
import az.ingress.ms9liquibase.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Set;

@SpringBootApplication
@RequiredArgsConstructor
public class Ms9LiquibaseApplication implements CommandLineRunner {

	private final UserRepository userRepository;
	public static void main(String[] args) {
		SpringApplication.run(Ms9LiquibaseApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		User user=new User();
		user.setUsername("azer");
		user.setPassword("12345");

		Address address=new Address();
		address.setName("Baki");
		address.setContact("www.academy.az");

		user.setAddress(address);

		Contact contact1=new Contact();
		contact1.setNumber("3762828");
		Contact contact2=new Contact();
		contact2.setNumber("4500575");

		UserRole userRole1=new UserRole();
		userRole1.setRole("admin");
		UserRole userRole2=new UserRole();
		userRole2.setRole("eliteUser");
		Set<UserRole> userRoles= Set.of(userRole1,userRole2);

		Set<Contact> contact_numbers=Set.of(contact2,contact1);
		address.setContact_numbers(contact_numbers);
		user.setUserRoles(userRoles);

		userRepository.save(user);
	}
}
