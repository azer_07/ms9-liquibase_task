package az.ingress.ms9liquibase.controller;

import az.ingress.ms9liquibase.dto.ProductDto;
import az.ingress.ms9liquibase.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/products")
public class ProductController {
    private final ProductService service;
    @GetMapping("/{id}")
    public ProductDto getProductById(@PathVariable Long id){
        return service.getProductById(id);
    }
    @PostMapping
    public ProductDto createProduct(@RequestBody ProductDto dto){
        return service.createProduct(dto);
    }
    @PutMapping
    public ProductDto updateProduct(@RequestBody ProductDto dto){
        return service.updateProduct(dto);
    }
    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable Long id){
        service.deleteProduct(id);
    }
}
