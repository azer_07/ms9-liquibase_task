package az.ingress.ms9liquibase.dto;

import lombok.Data;

@Data
public class ProductDto {
    private Long id;
    private String uniqueCode;
    private Integer price;
    private String createDate;
    private String brand;
    private Integer count;
    private Boolean isActive;

}
